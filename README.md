# README #

* **Co działa:**
*   dane się wyświetlają
*   można je filtrować
*   paginacja od 10 dodanych rekordów
*   można przejść flow
*   treść da się zmienić przy stanach 'verified' i 'created'
*   można nadać unikalny identyfikator numeryczny (ręcznie)

* **Inne:**
*   Brak testów automatycznych
*   Tylko jeden przykładowy test został dodany.