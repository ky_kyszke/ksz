package it.head.pms.model.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@MappedSuperclass
@NoArgsConstructor
public abstract class BaseEntity<ID_TYPE> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	ID_TYPE id;
	
	@Version
	Long version;
}
