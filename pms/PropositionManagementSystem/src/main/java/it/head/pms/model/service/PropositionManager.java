package it.head.pms.model.service;

/**
 * Wrapper service leveraging on {@link Repository Repositories}
 * 
 * @author Jakub Hetmanski
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.head.pms.model.dao.PropositionRepository;
import it.head.pms.model.dao.PropositionStateTransitionRepository;
import it.head.pms.model.dao.PublishedPropositionRepository;
import it.head.pms.model.domain.Proposition;
import it.head.pms.model.domain.PropositionState;
import it.head.pms.model.domain.PropositionStateTransition;
import it.head.pms.model.domain.PropositionStateTransitionWithReason;
import it.head.pms.model.domain.PublishedPropositionExternalId;
@Transactional(readOnly = false)
@Service
public class PropositionManager {
	
	@Autowired
	private PropositionRepository propositionRepository;
	
	@Autowired
	private PublishedPropositionRepository publishedPropositionRepository;
	
	@Autowired
	private PropositionStateTransitionRepository stateTransitionRepository;
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Proposition save(Proposition proposition) {
		return propositionRepository.save(proposition);
	}
	
	/** 
	 * Adding new {@link Proposition} object. 
	 */
	public Proposition add(String name, String content) {
		return addTransition(new Proposition(name, content), PropositionState.CREATED, null); 
	}
	
	/** 
	 * Marks the{@link Proposition} as {@link PropositionState#DELETED} and sets up 
	 * the {@link PropositionStateTransition} mnemonic. 
	 */
	public void delete(Proposition proposition, String reason) {
		addTransition(proposition, PropositionState.DELETED, reason);
	}

	/** 
	 * Marks the{@link Proposition} as {@link PropositionState#VERIFIED} and sets up 
	 * the {@link PropositionStateTransition} mnemonic.
	 */
	public void verify(Proposition proposition) {
		addTransition(proposition, PropositionState.VERIFIED);
	}
	
	/** 
	 * Marks the{@link Proposition} as {@link PropositionState#ACCEPTED} and sets up 
	 * the {@link PropositionStateTransition} mnemonic.
	 */
	public void accept(Proposition proposition) {
		addTransition(proposition, PropositionState.ACCEPTED);
	}
	
	/** 
	 * Marks the{@link Proposition} as {@link PropositionState#REJECTED} and sets up 
	 * the {@link PropositionStateTransition} mnemonic.
	 */
	public void reject(Proposition proposition, String reason) {
		addTransition(proposition, PropositionState.REJECTED, reason);
	}
	
	/** 
	 * Marks the{@link Proposition} as {@link PropositionState#PUBLISHED} and sets up 
	 * the {@link PropositionStateTransition} mnemonic.
	 */
	public void publish(Proposition proposition, PublishedPropositionExternalId publishedProposition) {
		publishedPropositionRepository.save(publishedProposition);
		addTransition(proposition, PropositionState.PUBLISHED);
	}

	/* DAO / Repositories wrappers */
	
	/** 
	 * Get by ID 
	 */
	@Transactional(readOnly = false)
	public Proposition findOneProposition(Long id) {
		return propositionRepository.findOne(id);
	}
	
	/** 
	 * Find all the {@link Proposition} records
	 */
	@Transactional(readOnly = false)
	public List<Proposition> findAllPropositions() {
		return propositionRepository.findAll();
	}
	
	/**
	 * Retrieves the history of {@link PropositionState proposition's state} changes
	 * @param proposition
	 * @return the list of {@link PropositionStateTransition}
	 */
	@Transactional(readOnly = false)
	public List<PropositionStateTransition> getHistoryOfChanges(Proposition proposition) {
		return stateTransitionRepository.findByPropositionOrderByTimestampAsc(proposition);
	}
	
	private Proposition addTransition(Proposition proposition, PropositionState toState) {
		return addTransition(proposition, toState, null);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private Proposition addTransition(Proposition proposition, PropositionState toState, String reason) {
		proposition.setState(toState);
		
		Proposition savedProposition = saveProposition(proposition);
		
		stateTransitionRepository.save(reason == null ?
				new PropositionStateTransition(toState, savedProposition)
				: new PropositionStateTransitionWithReason(toState, savedProposition, reason)
		);
		
		return savedProposition;
	}
	
	private Proposition saveProposition(Proposition proposition) {
		return propositionRepository.save(proposition);
	}

	
}
