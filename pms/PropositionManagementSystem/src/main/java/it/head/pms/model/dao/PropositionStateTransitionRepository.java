package it.head.pms.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import it.head.pms.model.domain.Proposition;
import it.head.pms.model.domain.PropositionStateTransition;

public interface PropositionStateTransitionRepository
		extends PagingAndSortingRepository<PropositionStateTransition, Long> {
	
	List<PropositionStateTransition> findAll();
	
	List<PropositionStateTransition> findByPropositionOrderByTimestampAsc(Proposition proposition);

}
