package it.head.pms.model.domain;
/**
 * Enum with possible {@link Proposition Propositions'} states.
 * 
 * @author Jakub Hetmanski
 */
public enum PropositionState {
	
	/**
	 * Created object could be either deleted or verified.
	 * The content of the proposition can be changed in this state.
	 */
	CREATED(1),
	
	/**
	 * Deleted state cannot be changed.
	 * Proposition has been deleted.
	 */
	DELETED(2),
	
	/**
	 * Verified object can be either accepted or rejected.
	 * User is able to change the content of the proposition.
	 */
	VERIFIED(3),
	
	/**
	 * Rejected state cannot be changed. 
	 * Proposition has been rejected.
	 */
	REJECTED(4),
	
	/**
	 * From this state user can either remove
	 * reject the proposition or publish it.
	 */
	ACCEPTED(5),
	
	/**
	 * Published state cannot be changed.
	 * Proposition should be marked with the
	 * unique numeric ID.
	 */
	PUBLISHED(6);
		
	private int id;

	public int getId() {
		return id;
	}

	private PropositionState(int id) {
		this.id = id;
	}
}
