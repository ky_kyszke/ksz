package it.head.pms.model.domain;

import java.time.LocalDateTime;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Required;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * {@link PropositionStateTransition} with mandatory {@link #reason} property.
 * 
 * During the transitions to state: {@link PropositionState#DELETED} and to 
 * state: {@link PropositionState#REJECTED} the reason for that change has to be provided.
 * 
 * @author Jakub Hetmanski
 */
@SuppressWarnings("unused")
@DiscriminatorValue("1")
@NoArgsConstructor
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class PropositionStateTransitionWithReason extends PropositionStateTransition {
	
	@NotEmpty
	@Length(max = 255)
	private String reason;

	public PropositionStateTransitionWithReason(PropositionState toState, Proposition proposition, String reason) {
		super(toState, proposition);
		this.reason = reason;
	}

}
