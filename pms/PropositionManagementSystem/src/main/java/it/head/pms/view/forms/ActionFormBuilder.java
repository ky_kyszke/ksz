package it.head.pms.view.forms;

import java.util.EnumSet;

import com.vaadin.data.util.converter.Converter.ConversionException;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import it.head.pms.model.domain.Proposition;
import it.head.pms.model.domain.PropositionState;
import it.head.pms.model.domain.PublishedPropositionExternalId;
import it.head.pms.model.service.PropositionManager;
import it.head.pms.view.forms.ActionForm.ChangeHandler;

class ActionFormBuilder extends VerticalLayout implements View {

	private static final long serialVersionUID = -4618949841131566960L;

	private final Proposition proposition;
	
	private final PropositionManager propositionManager;
	
	private final ChangeHandler changeHandler;
	
	private final TextField uniqueIdentifier = new TextField("Unique identity number:");

	public ActionFormBuilder(Proposition proposition, PropositionManager propositionManager, ChangeHandler changeHandler) {
		this.proposition = proposition;
		this.propositionManager = propositionManager;
		this.changeHandler = changeHandler;
		createView(proposition);
	}
	
	protected final void createView(final Proposition proposition) {
		
		PropositionState forState = proposition.getState();
		
		final TextField name = new TextField("Application title:");
		final TextField content = new TextField("Application text:");
		final TextField reason = new TextField();
		final CssLayout buttons = new CssLayout();
		final HorizontalLayout inputs = new HorizontalLayout();
		
		// we are adding new proposition
		if (forState == null) {
			inputs.addComponents(name);
			buttons.addComponents(getStateTransitionWithParamsButton(PropositionState.CREATED, name, content));
		}
		
		if (forState == PropositionState.CREATED || forState == PropositionState.VERIFIED || forState == null) {
			// content is changable in state CREATED, VERIFIED and at creation time
			String contentText = proposition.getContent();
			
			content.setValue(contentText != null ? contentText : "");
			content.setBuffered(false);
			content.addTextChangeListener(evt -> proposition.setContent(evt.getText()));
			inputs.addComponents(content);
		}
		
		if (PropositionState.ACCEPTED == forState) {
			uniqueIdentifier.setMaxLength(PublishedPropositionExternalId.MAX_LENGTH);
			uniqueIdentifier.setConverter(Long.class);
			uniqueIdentifier.setValue("");
			inputs.addComponent(uniqueIdentifier);
		}
		
		if (EnumSet.of(PropositionState.CREATED, PropositionState.VERIFIED, PropositionState.ACCEPTED).contains(forState)) {
			// we need reason for delete and reject action
			if (forState == PropositionState.CREATED) {
				reason.setCaption("Reason for removal");
			} else {
				reason.setCaption("Reason for rejection");
			}
			inputs.addComponent(reason);
		}
		
		if (EnumSet.of(PropositionState.VERIFIED, PropositionState.ACCEPTED).contains(forState)) {
			buttons.addComponent(getStateTransitionWithParamsButton(PropositionState.REJECTED, reason));
		}
		
		// state specific buttons
		if (forState != null) {
			switch(forState) {
			case CREATED:
				buttons.addComponents(
						getStateTransitionWithParamsButton(PropositionState.DELETED, reason),
						getStateTransitionButton(PropositionState.VERIFIED));
				break;
			case VERIFIED:
				buttons.addComponents(getStateTransitionButton(PropositionState.ACCEPTED));
				break;
			case ACCEPTED:
				buttons.addComponent(getStateTransitionButton(PropositionState.PUBLISHED));
				break;
			default:
			}
		}
		
		buttons.iterator().forEachRemaining(comp -> {
			if (comp instanceof Button) {
				((Button) comp).addClickListener(e -> changeHandler.onChange());
			}
		});
		
		addComponents(inputs, buttons);
	}
	
	protected final Button getStateTransitionButton(PropositionState transitionToState) {
		Button stateTransisionButton;
		
		switch(transitionToState) {
		case ACCEPTED:
			stateTransisionButton = new Button("Accept", FontAwesome.THUMBS_O_UP);
			stateTransisionButton.addClickListener(evt -> propositionManager.accept(proposition));
			break;
		case VERIFIED:
			stateTransisionButton = new Button("Verify", FontAwesome.CHECK_CIRCLE_O);
			stateTransisionButton.addClickListener(evt -> propositionManager.verify(proposition));
			break;
		case PUBLISHED:
			stateTransisionButton = new Button("Publish", FontAwesome.UPLOAD);
			stateTransisionButton.addClickListener(evt -> {		
				PublishedPropositionExternalId pp = new PublishedPropositionExternalId(proposition);
				try {
					Long uniqueLong = (Long) uniqueIdentifier.getConvertedValue();
					pp.setExternalId(String.valueOf(uniqueLong));				
				} catch (ConversionException e) {
					throw e;
				}
				propositionManager.publish(proposition, pp);
			});
			break;
			default:
				throw new UnsupportedOperationException();
		}
		
		return stateTransisionButton;
	}
	
	protected final Button getStateTransitionWithParamsButton(PropositionState transitionToState, TextField... inputs) {
		Button stateTransisionButton;
		
		switch(transitionToState) {
		case DELETED:
			stateTransisionButton = new Button("Delete", FontAwesome.REMOVE);
			stateTransisionButton.addClickListener(evt -> propositionManager.delete(proposition, inputs[0].getValue()));
			break;
		case REJECTED:
			stateTransisionButton = new Button("Reject", FontAwesome.THUMBS_O_DOWN);
			stateTransisionButton.addClickListener(evt -> propositionManager.reject(proposition, inputs[0].getValue()));
			break;
		case CREATED:
			stateTransisionButton = new Button("Save Application", FontAwesome.SAVE);
			stateTransisionButton.addClickListener(evt -> { 
				propositionManager.add(inputs[0].getValue(), inputs[1].getValue());
			});
			break;
			default:
				throw new UnsupportedOperationException();
		}
		
		return stateTransisionButton;
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}
}
