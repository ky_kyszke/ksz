package it.head.pms.view;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.Repository;
import org.vaadin.pagingcomponent.Page;
import org.vaadin.pagingcomponent.PagingComponent;
import org.vaadin.pagingcomponent.PagingComponent.ChangePageEvent;

import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import it.head.pms.model.domain.Proposition;
import it.head.pms.model.domain.PropositionState;
import it.head.pms.model.domain.PropositionStateTransition;
import it.head.pms.model.service.PropositionManager;
import it.head.pms.view.forms.ActionForm;
import it.head.pms.view.forms.ActionForm.ChangeHandler;

/**
 * Main application screen implemented with Vaadin
 * 
 * @author Jakub Hetmanski
 */
@SpringUI
public class PmsUI extends UI {
	

	private static final String FILTER_BY_STATE_PROMPT = "filter by state";

	private static final String FILTER_BY_NAME_PROMPT = "filter by name";

	private static final long serialVersionUID = -2498486720074090177L;

	/* Constants */
	
	/** Max items per page **/
	private static final int ROWS_PER_PAGE = 10;
	/** Propositions' list caption **/
	private static final String PROPOSITION_LIST_CAPTION = "Applications' List";
	/** History of state's changes **/
	private static final String STATE_HISTORY_LIST_CAPTION = "History of changes";
	/** Create new proposition button's label **/
	private static final String ADD_NEW_PROPOSAL = "New application";
	
	/* UI controls */
	
	/* Propositions related components */
	
	/** Grid for Propositions **/
	private final Grid propositions = new Grid(PROPOSITION_LIST_CAPTION);
	/** Actual bean's container **/
	private final BeanContainer<Long, Proposition> propositionsContainer = new BeanContainer<>(Proposition.class);
	/** Paginator for {@link #propositions propositions'} grid **/
	private PagingComponent<Proposition> paginatorForPropositions;
	
	/* History of changes */
	
	/** Another grid. This time for mnemonic **/
	private final Grid statesChangesHistory = new Grid(STATE_HISTORY_LIST_CAPTION);
	
	/** Service wrapping {@link Repository Repositories} **/ 
	private final PropositionManager propositionManager;

	/** Add new item button **/
	private final Button addNew = new Button(ADD_NEW_PROPOSAL);
	
	private final ActionForm contextActionForm;
	
	private final VerticalLayout contextFormRoot = new VerticalLayout();
	
	private final ChangeHandler changeHandler = new ChangeHandler() {
		@Override
		public void onChange() {
			contextActionForm.showView(new Navigator.EmptyView());
			paginatorForPropositions = initPaginator(propositionManager.findAllPropositions(), propositionsContainer);
			Collection<Object> selection = propositions.getSelectedRows();
			// hack for re-initializing the context form
			propositions.fireSelectionEvent(selection, selection);
		}
	};
	
	@Autowired
	public PmsUI(PropositionManager propositionManager) {
		this.propositionManager = propositionManager;
		contextActionForm = new ActionForm(contextFormRoot, propositionManager);
	}

	@Override
    protected void init(VaadinRequest request) {
		// both grids initialization
		setupProposalsGrid();
		setupStatesChangesHistory();
		
		// init container & paginator
		propositionsContainer.setBeanIdResolver(proposition -> proposition.getId());
		// paginator will fuel the container with some data
		paginatorForPropositions = initPaginator(propositionManager.findAllPropositions(), propositionsContainer);
		
		// add new button click handler
		addNew.addClickListener(clickEvent -> {
			contextActionForm.showForm(new Proposition(), changeHandler);
		});
		
		// layout setup
		HorizontalLayout buttons = new HorizontalLayout(addNew);
		VerticalLayout propTableWithPaginator = new VerticalLayout(propositions, paginatorForPropositions);
		HorizontalLayout tables = new HorizontalLayout(propTableWithPaginator, statesChangesHistory);
		VerticalLayout layout = new VerticalLayout(buttons, tables, contextFormRoot);
		
		layout.setMargin(true);
		layout.setSpacing(true);
		
		setContent(layout);
    }
	
	private void setupProposalsGrid() {
		propositions.setSelectionMode(SelectionMode.SINGLE);
		propositions.setImmediate(true);
		propositions.setFooterVisible(true);
		propositions.setHeightMode(HeightMode.ROW);
		propositions.setHeightByRows(ROWS_PER_PAGE);
		propositions.setContainerDataSource(propositionsContainer);
		propositions.setColumnOrder(Proposition.NAME, Proposition.CONTENT, Proposition.STATE);
		propositions.addSelectionListener(selEvent -> {
			
			Optional<Object> eventSource = selEvent.getSelected().stream().findFirst();
			
			if (eventSource.isPresent() && eventSource.get() instanceof Long) {
				Proposition prop = propositionsContainer.getItem(eventSource.get()).getBean();
				
				statesChangesHistory.setContainerDataSource(
						new BeanItemContainer<>(PropositionStateTransition.class,
								propositionManager.getHistoryOfChanges(prop)));
				statesChangesHistory.setVisible(true);
				
				contextActionForm.showForm(prop, changeHandler);
			}
		});
		
		setupFilters();
	}

	private void setupFilters() {
		// setup filter for 'name' property
		HeaderRow filterRow = propositions.appendHeaderRow();
		TextField namFilter = new TextField();
		namFilter.setInputPrompt(FILTER_BY_NAME_PROMPT);
		namFilter.addTextChangeListener(changeEvent -> {
			propositionsContainer.removeContainerFilters(Proposition.NAME);
		
			if (!changeEvent.getText().isEmpty()) {
				propositionsContainer.addContainerFilter(new SimpleStringFilter(Proposition.NAME, changeEvent.getText(), true, false));
			}
		});
		filterRow.getCell(Proposition.NAME).setComponent(namFilter);
		
		// setup filter for 'state' property
		ComboBox staFilter = new ComboBox();
		staFilter.setInputPrompt(FILTER_BY_STATE_PROMPT);
		staFilter.setContainerDataSource(new IndexedContainer(EnumSet.allOf(PropositionState.class)));
		staFilter.addValueChangeListener(valueChangeEvent -> {
			propositionsContainer.removeContainerFilters(Proposition.STATE);
			if (valueChangeEvent.getProperty().getValue() != null) {
				propositionsContainer.addContainerFilter(new SimpleStringFilter(Proposition.STATE, valueChangeEvent.getProperty().getValue().toString(), true, false));
			}
		});
		filterRow.getCell(Proposition.STATE).setComponent(staFilter);
	}
	
	private void setupStatesChangesHistory() {
		statesChangesHistory.setVisible(false);
    	propositions.setImmediate(true);
    	propositions.setFooterVisible(true);
    	propositions.setHeightMode(HeightMode.ROW);
    	propositions.setHeightByRows(ROWS_PER_PAGE);
		
	}
	
	private <ID_TYPE, BEAN_TYPE> PagingComponent<BEAN_TYPE> initPaginator(Collection<BEAN_TYPE> propositions, BeanContainer<ID_TYPE, BEAN_TYPE> container) {
		return PagingComponent.paginate(propositions)
					.numberOfItemsPerPage(ROWS_PER_PAGE)
					.page(Page.FIRST)
					.addListener((ChangePageEvent<BEAN_TYPE> e) -> {
						container.removeAllItems();
						container.addAll(e.getPageRange().getItemsList());
					}).build();
	}
}


	