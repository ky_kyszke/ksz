package it.head.pms.view.forms;

import com.vaadin.navigator.Navigator.ComponentContainerViewDisplay;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.VerticalLayout;

import it.head.pms.model.domain.Proposition;
import it.head.pms.model.service.PropositionManager;

@UIScope
@SpringComponent
public class ActionForm extends ComponentContainerViewDisplay {
	
	private static final long serialVersionUID = 3952767497583586034L;
	
	public interface ChangeHandler {
		void onChange();
	}
	
	private final PropositionManager propositionManager;
	
	private Proposition proposition;

	public ActionForm(VerticalLayout compositionRoot, PropositionManager propositionManager) {
		super(compositionRoot);
		this.propositionManager = propositionManager;
	}
	
	public void showForm(Proposition proposition, ChangeHandler changeHandler) {
		
		final boolean persisted = proposition.getId() != null;
		if (persisted) {
			// Find fresh entity for editing
			this.proposition = propositionManager.findOneProposition(proposition.getId());
		}
		else {
			this.proposition = proposition;
		}
		
		showView(new ActionFormBuilder(this.proposition, propositionManager, changeHandler));
	}
}
