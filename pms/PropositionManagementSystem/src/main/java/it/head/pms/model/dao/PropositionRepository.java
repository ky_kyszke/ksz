package it.head.pms.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import it.head.pms.model.domain.Proposition;

/**
 * Repository interface for querying the {@link Proposition} table. 
 * 
 * @author kyszke
 */
public interface PropositionRepository extends PagingAndSortingRepository<Proposition, Long> {	
	
	/** 
	 * Select all 
	 */
	List<Proposition> findAll();
}
