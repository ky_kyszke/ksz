package it.head.pms.model.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import it.head.pms.model.domain.PublishedPropositionExternalId;

public interface PublishedPropositionRepository extends PagingAndSortingRepository<PublishedPropositionExternalId, Long> {

}
