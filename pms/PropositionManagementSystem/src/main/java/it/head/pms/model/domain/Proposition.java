package it.head.pms.model.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@SequenceGenerator(name = "PROP_SEQ")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Proposition extends BaseEntity<Long> implements Serializable {
	
	private static final long serialVersionUID = 5144739798269265098L;
	
	public static final String NAME = "name";
	public static final String CONTENT = "content";
	public static final String STATE = "state";
	
	public Proposition(String name, String content) {
		this.name = name;
		this.content = content;
	}
	
	@NotBlank
	private String name;
	
	@NotBlank
	private String content;
	
	private PropositionState state;
	
	@PrePersist
	public void prePersist() {
		if (state == null) {
			state = PropositionState.CREATED;
		}
	}
}
