package it.head.pms.model.domain;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.datetime.joda.LocalDateTimeParser;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@SuppressWarnings("unused")
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "REASON_REQUIRED", discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue("0")
@SequenceGenerator(name = "PROP_STATE_TRANS_SEQ")
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class PropositionStateTransition extends BaseEntity<Long> {

	public PropositionStateTransition(PropositionState toState, Proposition proposition) {
		this.toState = toState;
		this.proposition = proposition;
	}

	@CreationTimestamp
	private Timestamp timestamp;
	
	private PropositionState toState;
	
	@ManyToOne
	private Proposition proposition;
}
