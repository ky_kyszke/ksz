package it.head.pms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"it.head.pms.*"})
public class PropositionManagementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(PropositionManagementSystemApplication.class, args);
	}
	
//	@Bean
//	public CommandLineRunner loadData(final PropositionManager propositionManager) {
//		return (args) -> {
//			for (int i = 0; i < 12; i++) {
//				propositionManager.add("name" + i, "content" + i);
//			}
//		};
//	}
}
