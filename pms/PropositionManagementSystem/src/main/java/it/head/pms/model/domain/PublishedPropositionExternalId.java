package it.head.pms.model.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.NumberFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class PublishedPropositionExternalId extends BaseEntity<Long> implements Serializable {
	private static final long serialVersionUID = -6885398636586814968L;

	public static final int MAX_LENGTH = 9;
	
	@OneToOne(orphanRemoval = true)
	private Proposition proposition;

	@NumberFormat
	@Length(min = 1, max = MAX_LENGTH)
	@Column(precision = MAX_LENGTH, unique = true)
	private String externalId;
	
	public PublishedPropositionExternalId(Proposition proposition) {
		this.proposition = proposition;
	}
}
