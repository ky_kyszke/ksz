package it.head.pms.view.forms;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import it.head.pms.model.domain.Proposition;
import it.head.pms.model.service.PropositionManager;
import it.head.pms.view.forms.ActionForm.ChangeHandler;

@RunWith(MockitoJUnitRunner.class)
public class ActionFormTest {

	@Mock
	private PropositionManager propositionManager;
	
	@Mock
	private ChangeHandler changeHandler;
	
	private VerticalLayout rootElement = new VerticalLayout();
	
	private ActionForm testee;
	
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(getClass());
		testee = new ActionForm(rootElement, propositionManager);
	}
	
	/** 
	 * Initialization of testee with fresh Proposition entity.
	 * That should generate the ActionForm for transition to state CREATED
	 */
	@Test
	public void testFormForCreateAction() {
		// fresh entity
		testee.showForm(new Proposition(), changeHandler);
		
		// root container should contain only one element of type ActionFormBuilder
		assertThat(rootElement.getComponentCount()).isEqualTo(1);
		assertThat(rootElement.getComponent(0)).isInstanceOf(ActionFormBuilder.class);
		ActionFormBuilder actionFormFactory = (ActionFormBuilder) rootElement.getComponent(0);
		
		// actionForm should contain two containers: one for buttons and one for inputs
		assertThat(actionFormFactory.getComponentCount()).isEqualTo(2);
		assertThat(actionFormFactory.getComponent(0)).isInstanceOf(HorizontalLayout.class);
		HorizontalLayout inputs = (HorizontalLayout) actionFormFactory.getComponent(0);
		assertThat(actionFormFactory.getComponent(1)).isInstanceOf(CssLayout.class);
		CssLayout buttons = (CssLayout) actionFormFactory.getComponent(1);
		
		// save button should be present within the buttons container
		assertThat(buttons.getComponentCount()).isEqualTo(1);
		Button actualSaveButton = (Button) buttons.getComponent(0);
		assertThat(actualSaveButton.getCaption()).isEqualTo("Save Application");
		assertThat(actualSaveButton.getIcon()).isEqualTo(FontAwesome.SAVE);
		
		// .. and two inputs for entities' name and content properties setup
		TextField name = (TextField) inputs.getComponent(0);
		TextField content = (TextField) inputs.getComponent(1);
		
		/**
		 * Actual test
		 */
		
		// given:
		name.setValue("TEST NAME");
		content.setValue("TEST CONTENT");
		
		// when:
		actualSaveButton.click();
		
		// then:
		
		/* the propositionManager with should me called using the values from the input fields */ 
		verify(propositionManager).add("TEST NAME", "TEST CONTENT");
		verify(changeHandler).onChange();
	}
	
}
