package it.head.pms;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PropositionManagementSystemApplication.class)
@WebAppConfiguration
public class PropositionManagementSystemApplicationTests {

	@Test
	public void contextLoads() {
	}

}
